<?php
/**
 * Plugin Name: NJPW Wordpress Plugin
 * Plugin URI: https://gitlab.com/Bacon_Space/NJPW-WP-Plugin
 * Description: This is a plugin that provides information on how to watch New Japan Pro Wrestling. Shortcode [njpw]
 * Version: 1.0.0
 * Author: Bacon_Space
 * Author URI: https://twitter.com/Bacon_Space
 */


function njpw() {
   
    // Additional information about where to watch NJPW
    $output .= "<h4>Watch New Japan Pro Wrestling</h4>";
    $output .= "<ul>";
    $output .= "<li>USA: Fridays 8pm ET on <a href='https://www.njpwworld.com/' target='_blank'>NJPW World</a>, Saturdays 8pm ET on <a href='https://www.premiersports.com/' target='_blank'>Premier Sports</a> (channel 545 on Sky, channel 435 on Virgin, channel 551 on Premier Player), and on the <a href='https://channelstore.roku.com/details/435370/new-japan-pro-wrestling' target='_blank'>New Japan Pro Wrestling Roku Channel</a></li>";
    $output .= "<li>Canada: Fridays 8pm ET on <a href='https://www.fite.tv/' target='_blank'>FITE TV</a>, Saturdays 8pm ET on <a href='https://www.premiersports.com/' target='_blank'>Premier Sports</a> and on the <a href='https://channelstore.roku.com/details/435370/new-japan-pro-wrestling' target='_blank'>New Japan Pro Wrestling Roku Channel</a></li>";
    $output .= "</ul>";
    
    return $output;
}

add_shortcode('njpw', 'njpw');